Sporządzić raport (sprawozdanie), w którym zostaną umieszczone wyniki uzyskane przy użyciu narzędzi zaimplementowanych w zadaniach 1 i 2.
Dane do wykorzystania: irisdat.txt, income.txt, arrhythmia.txt

Należy przeprowadzić następujące eksperymenty:
- (DONE) dyskretyzacja (przedziały równej długości) - zbiór arrhythmia.txt, atrybut 1 (wiek pacjenta), 10 i 5 przedziałów
- (DONE) standaryzacja (normalizacja) - zbiór irisdat.txt, atrybut 2 (szerokość listków)
- (DONE) standaryzacja (przedział min-max) - zbiór income.txt, atrybut 1 i 2, przedział 0-10
- (DONE)(DOPRACOWC KOD) klasyfikacja k-nn - zbiory irisdat.txt, income.txt, arrhythmia.txt,
- (DONE) pokazać na wykresie zależność
    * jakości klasyfikatora mierzonej metodą leave-one-out od liczby sąsiadów
    * dla różnych metryk oceny odległości,
    * liczba sąsiadów 1 do n-1, gdzie n-liczność zbioru danych


Raport powinien zawierać:
- krótka część teoretyczna - opis używanych technik przetwarzania i eksploracji danych
- krótka część techniczna - opis sposobu implementacji powyższych technik
- wyniki eksperymentów
- wnioski - analiza wyników, wady i zalety zastosowanych technik