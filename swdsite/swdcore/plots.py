import plotly.graph_objs as go
import plotly.offline as py
import matplotlib.pyplot as plp
import mpld3

colors = {1: 'skyblue', 2: 'springgreen', 3: 'tomato', 4: 'orange', 5: 'violet', 6: 'yellow', 7: 'deeppink'}


class SWDPlot:
    def __init__(self, x_and_y_data_set, plot_labels, plot_mode, plot_name):
        self.x_axis_data, self.y_axis_data = x_and_y_data_set
        self.plot_labels = plot_labels
        self.plot_name = plot_name
        self.plot_mode = plot_mode

    def display_plot_info(self):
        print("Plot " + self.plot_name + " with labels: " + self.plot_labels + " in mode: " + self.plot_mode)


def create_2d_scattered_plot_for_1_variable(data_set, plot_mode, plot_name):
    labels = data_set.columns.values
    scope = list(range(data_set.shape[0]))
    x_axis_data = [scope for label in labels]
    y_axis_data = [data_set[label] for label in labels]
    x_and_y_data_set = (x_axis_data, y_axis_data)
    swd_plot = SWDPlot(x_and_y_data_set=x_and_y_data_set,
                       plot_labels=labels,
                       plot_mode=plot_mode,
                       plot_name=plot_name)
    return create_2d_scattered_plot(swd_plot)


def create_2d_scattered_plot_for_2_variables(data_set, labels, variables, plot_mode):
    all_decision_class_values = data_set[variables['decision_class']]
    attr_x = variables['attr_x']
    attr_y = variables['attr_y']
    axis_data = [data_set.loc[all_decision_class_values == label] for label in labels]
    x_axis_data = [data[attr_x] for data in axis_data]
    y_axis_data = [data[attr_y] for data in axis_data]
    x_and_y_data_set = (x_axis_data, y_axis_data)
    plot_name = attr_x + " : " + attr_y + " for " + variables['decision_class']
    swd_plot = SWDPlot(x_and_y_data_set=x_and_y_data_set,
                       plot_labels=labels,
                       plot_mode=plot_mode,
                       plot_name=plot_name)
    return create_2d_scattered_plot(swd_plot)


def create_2d_scattered_plot(swd_plot):
    graph_traces = []
    for idx, label in enumerate(swd_plot.plot_labels):
        t = go.Scatter(
            x=swd_plot.x_axis_data[idx],
            y=swd_plot.y_axis_data[idx],
            mode=swd_plot.plot_mode,
            marker=dict(color=colors[idx + 1]),
            name=str(label)
        )
        graph_traces.append(t)
    layout = go.Layout(title=swd_plot.plot_name)
    fig = go.Figure(data=graph_traces, layout=layout)
    scattered_graph = py.plot(fig, output_type='div', include_plotlyjs=False)
    return scattered_graph


def create_3d_scattered_graph():
    pass


def create_linear_graph():
    pass


def create_bar_graph(data_set):
    bar_graphs = {}
    for idx, col in enumerate(data_set.columns.values):
        data_set[col].value_counts(sort=False).plot(kind='bar', color=[colors.get(idx + 1)])
        bar_graph = mpld3.fig_to_html(plp.gcf())
        plp.close(plp.gcf())
        bar_graphs['graph_' + col.lower()] = bar_graph
    return bar_graphs


def create_knn_plots(data_set, plot_mode):
    labels_distinct_values = data_set[data_set.columns[-1]].unique()
    graphs_knn = []
    for idx, var in enumerate(data_set.columns.values):
        if idx % 2 == 1:
            cols = data_set.columns
            variables = {'decision_class': cols[-1], 'attr_x': cols[idx - 1], 'attr_y': cols[idx]}
            plot = create_2d_scattered_plot_for_2_variables(data_set, labels_distinct_values, variables, plot_mode)
            graphs_knn.append(plot)
    return graphs_knn


def create_plot_from_dict(mapa):
    plp.plot(*zip(*sorted(mapa.items())))
    plp.xlabel('Liczba sąsiadów')
    plp.ylabel('Dokładność')
    axes = plp.gca()
    axes.set_xlim(left=0)
    axes.set_ylim([0, 1])
    line_graph = mpld3.fig_to_html(plp.gcf())
    plp.close(plp.gcf())
    return line_graph

def create_plot_k_means(km):
    colors = 100 * ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    for centroid in km.centroids:
        plp.scatter(km.centroids[centroid][0], km.centroids[centroid][1], s=130, marker="x")

    for classification in km.classes:
        color = colors[classification]
        for features in km.classes[classification]:
            plp.scatter(features[0], features[1], color=color, s=30)

    plp.show()
