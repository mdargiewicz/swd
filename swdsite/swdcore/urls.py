from django.urls import path

from . import forms
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('', forms.SWDForm, name='SWDForm'),
    path('knn', views.knn),
    path('discretize', views.discretize),
    path('normalize', views.normalize),
    path('kmean', views.k_means)
]