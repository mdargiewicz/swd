import operator
import numpy as np
from ipython_genutils.py3compat import xrange

from .metrics import count_distance


class KnnAlgorithm:
    def __init__(self, training_data_set, test_data_set, metric, k):
        self.x_train, self.y_train = training_data_set
        self.x_test, self.y_test = test_data_set
        self.metric = metric
        self.k = k

    def run_knn(self):
        output_classes = []
        z = np.vstack((self.x_train, self.x_test))
        if self.metric == 'mahalanobis':
            covariance = np.cov(z.T)
            inv_covariance = np.linalg.inv(covariance)
            self.__run_knn_for_each_test_case(output_classes, inv_covariance)
        else:
            self.__run_knn_for_each_test_case(output_classes)
        return output_classes

    def __run_knn_for_each_test_case(self, output_classes, inv_covariance=None):
        for i in xrange(0, self.x_test.shape[0]):
            output = self.__get_neighbors(self.x_test[i], inv_covariance)
            predicted_class = self.predict_knn_class(output)
            output_classes.append(predicted_class)

    def __get_neighbors(self, x_test_i, inv_covariance=None):
        distances = []
        neighbors = []
        for i in xrange(0, self.x_train.shape[0]):
            dist = count_distance(self.metric, self.x_train[i], x_test_i, inv_covariance)
            distances.append((i, dist))
        distances.sort(key=operator.itemgetter(1))
        for x in xrange(self.k):
            neighbors.append(distances[x][0])
        return neighbors

    def predict_knn_class(self, output):
        class_votes = {}
        for i in xrange(len(output)):
            if self.y_train[output[i]] in class_votes:
                class_votes[self.y_train[output[i]]] += 1
            else:
                class_votes[self.y_train[output[i]]] = 1
        sorted_votes = sorted(class_votes.items(), key=operator.itemgetter(1), reverse=True)
        return sorted_votes[0][0]

    def calculate_prediction_accuracy(self, predicted_labels):
        count = 0
        for i in xrange(len(predicted_labels)):
            if predicted_labels[i] == self.y_test[i]:
                count += 1
        print(count, len(predicted_labels))
        return float(count) / len(predicted_labels)
