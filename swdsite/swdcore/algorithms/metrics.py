import numpy as np


def count_distance(metric, x_train_data, x_test_data, inv_covariance=None):
    if metric == 'euclidean':
        return count_euclidean_distance(x_train_data, x_test_data)
    elif metric == 'manhattan':
        return count_manhattan_distance(x_train_data, x_test_data)
    elif metric == 'chebyshev':
        return count_infinity_distance(x_train_data, x_test_data)
    elif metric == 'mahalanobis':
        return count_mahalanobis_distance(x_train_data, x_test_data, inv_covariance)
    else:
        return count_absolute_distance(x_test_data, x_train_data)


# L2 distance
def count_euclidean_distance(vector_1, vector_2):
    return np.sqrt(np.sum(np.power(vector_1 - vector_2, 2)))


def count_absolute_distance(vector_1, vector_2):
    return np.sqrt(np.sum(np.absolute(vector_1 - vector_2)))


def count_manhattan_distance(vector_1, vector_2):
    return np.sum(np.abs(vector_1 - vector_2))


# Chebyshev distance
def count_infinity_distance(vector_1, vector_2):
    return max(abs(vector_1 - vector_2))


# sqrt(((v1-v2).T * S^(-1))*(v1-v2))
def count_mahalanobis_distance(vector_1, vector_2, inv_covariance):
    delta = (vector_1 - vector_2).reshape(1, 4)
    md = []
    for i in range(len(delta)):
        md.append(np.sqrt(np.dot(np.dot(delta, inv_covariance), np.transpose(delta))))
    return md
