import numpy as np
from .metrics import count_distance
from .similarity_metrics import similarity, jaccard_index, cosine_similarity

class K_Means:
    def __init__(self, k=3, metric='manhattan', tolerance=0.0001, max_iterations=500):
        self.k = k
        self.metric = metric
        self.tolerance = tolerance
        self.max_iterations = max_iterations

    def fit(self, data):
        self.centroids = {}
        for i in range(self.k):
            self.centroids[i] = data[i]

        for i in range(self.max_iterations):
            self.classes = {}
            for i in range(self.k):
                self.classes[i] = []

            for d in data:
                distances = [count_distance(self.metric, d[:-1], self.centroids[centroid][:-1]) for centroid in self.centroids]
                classification = distances.index(min(distances))
                self.classes[classification].append(d)

            previous = dict(self.centroids)

            for classification in self.classes:
                self.centroids[classification] = np.average(self.classes[classification], axis = 0)

            is_optimal = True

            for centroid in self.centroids:
                orginal_centroid = previous[centroid]
                current = self.centroids[centroid]

                if np.sum((current - orginal_centroid) / orginal_centroid * 100.0 ) > self.tolerance:
                    is_optimal = False

            if is_optimal:
                matched_0 = 0
                not_matched_0_1 = 0
                not_matched_0_2 = 0
                matched_1 = 0
                not_matched_1_0 = 0
                not_matched_1_2 = 0
                matched_2 = 0
                not_matched_2_0 = 0
                not_matched_2_1 = 0
                for feature in self.classes[0]:
                    if int(feature[-1]) == 0:
                        matched_0 += 1
                    elif int(feature[-1] == 1):
                        not_matched_0_1 += 1
                        # not_matched_1_0 += 1
                    else:
                        not_matched_0_2 += 1
                        # not_matched_2_0 += 1
                for feature in self.classes[1]:
                    if int(feature[-1]) == 1:
                        matched_1 += 1
                    elif int(feature[-1] == 0):
                        not_matched_1_0 += 1
                        # not_matched_0_1 += 1
                    else:
                        not_matched_1_2 += 1
                        # not_matched_2_1 += 1
                for feature in self.classes[2]:
                    if int(feature[-1]) == 2:
                        matched_2 += 1
                    elif int(feature[-1] == 0):
                        not_matched_2_0 += 1
                        # not_matched_0_2 += 1
                    else:
                        not_matched_2_1 += 1
                        # not_matched_1_2 += 1
                print(" c\o |   0   |   1   |   2   |")
                print(" 0   |   {}  |   {}  |   {}  |".format(matched_0, not_matched_0_1, not_matched_0_2))
                print(" 1   |   {}  |   {}  |   {}  |".format(not_matched_1_0, matched_1, not_matched_1_2))
                print(" 2   |   {}  |   {}  |   {}  |".format(not_matched_2_0, not_matched_2_1, matched_2))
                example_jaccard_index = jaccard_index(set(data[0]), set(data[0]))
                example_cosine_index = cosine_similarity(set(data[0]), set(data[0]))
                sim = similarity(data, np.array(self.classes[0]), 'jac')
                break

    def pred(self, data):
        distances = [np.linalg.norm(data - self.centroids[centroid]) for centroid in self.centroids]
        classification = distances.index(min(distances))
        return classification
