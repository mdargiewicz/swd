import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler


def convert_object_columns_into_numerical(data_set):
    for col in data_set.select_dtypes(include='object').columns:
        data_set[col] = pd.to_numeric(data_set[col])


def encode_labels_into_categorical_variables(data_set):
    data_set_objects_cols = data_set.select_dtypes(include='object').copy()
    data_set_cols_factorized = pd.DataFrame(
        pd.factorize(data_set_objects_cols.values.ravel())[0].reshape(data_set_objects_cols.shape),
        data_set_objects_cols.index,
        data_set_objects_cols.columns
    )
    data_set.update(data_set_cols_factorized)


def discretize_data_set(data_set, cups):
    data_set_copy = data_set.copy()
    for idx, col in enumerate(data_set.select_dtypes(exclude='object').columns):
        data_set_copy[col] = discretize_col(data_set_copy[col], cups)
    return data_set_copy


def discretize_col(col, cups_num):
    cups = cups_num
    return pd.cut(col, cups).to_frame()


def normalize_standard_deviation(data_set):
    data_set_normalization = standard_deviation_normalization_raw(data_set)
    data_set_normalization = pd.DataFrame(data=data_set_normalization,
                                          index=data_set.index,
                                          columns=data_set.columns)
    return data_set_normalization


def standard_deviation_normalization_raw(data_set):
    sc = StandardScaler()
    data_set_normalization = data_set.copy()
    sc.fit(data_set_normalization)
    data_set_normalization = sc.transform(data_set_normalization)
    return data_set_normalization


def normalize_min_max(data_set, min_param, max_param):
    sc = MinMaxScaler(feature_range=(min_param, max_param))
    data_set_normalization = data_set.copy()
    sc.fit(data_set_normalization)
    data_set_normalization = sc.transform(data_set_normalization)
    data_set_normalization = pd.DataFrame(data=data_set_normalization,
                                          index=data_set.index,
                                          columns=data_set.columns)
    return data_set_normalization


def generate_training_and_test_data(data_set):
    x = data_set.iloc[:, :-1].values
    y = data_set.iloc[:, -1].values
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
    x_train = standard_deviation_normalization_raw(x_train)
    x_test = standard_deviation_normalization_raw(x_test)
    training_data_set = (x_train, y_train)
    test_data_set = (x_test, y_test)
    return test_data_set, training_data_set
