import django_tables2 as tables
import numpy as np
import pandas as pd
from django.shortcuts import render
from django_tables2 import RequestConfig, Table

from . import plots as plots
from .algorithms import operations
from .algorithms.k_means_algorithm import K_Means
from .algorithms.knn_algorithm import KnnAlgorithm
from .forms import SWDForm

irisdat = 'swdcore/datasets/IRISDAT.txt'

context = {}


class CsvFileTable(Table):
    class Meta:
        attrs = {'class': 'table table-striped table-bordered table-hover'}


def index(request):
    context.clear()
    create_form(request)

    file = request.POST['file'] if request.method == 'POST' is not None else irisdat
    separator = request.POST['separator'] if request.method == 'POST' is not None else '\t'
    has_header = request.POST['has_header'] if request.method == 'POST' is not None else 'True'

    data_set = pd.read_csv(file, sep=separator, decimal=",", comment='#',
                           header='infer' if has_header == 'True' else None)
    context['data_set_var'] = data_set.columns.values if has_header == 'True' else ['col_' + str(idx) for idx, x in
                                                                                    enumerate(data_set.columns.values)]

    data_set.columns = context['data_set_var']

    context['data_set'] = data_set

    operations.encode_labels_into_categorical_variables(data_set)

    operations.convert_object_columns_into_numerical(data_set)

    # context['graph_basic'] = plots.create_2d_scattered_plot_for_1_variable(data_set, 'markers', 'graph_basic')

    create_table_with_data_from_file(data_set, request)

    return render(request, 'swdcore/index.html', context)


def normalize(request):
    data_set = context['data_set']
    type = request.GET['type']
    if type == 'std_dev':
        normalized_std_dev_data_set = operations.normalize_standard_deviation(data_set)
        context['graph_after_std_dev'] = plots.create_2d_scattered_plot_for_1_variable(normalized_std_dev_data_set,
                                                                                       'markers',
                                                                                       'graph_after_std_dev')
    elif type == 'min_max':
        min_value_scope = int(request.GET['min'])
        max_value_scope = int(request.GET['max'])
        normalized_min_max_data_set = operations.normalize_min_max(data_set, min_value_scope, max_value_scope)
        context['graph_after_min_max'] = plots.create_2d_scattered_plot_for_1_variable(normalized_min_max_data_set,
                                                                                       'markers',
                                                                                       'graph_after_min_max')
    else:
        print('Not supported')
    return render(request, 'swdcore/index.html', context)


def discretize(request):
    data_set = context['data_set']
    column = request.GET['columns']
    cups_num = int(request.GET['cups_num'])
    discretized_data_set = operations.discretize_col(data_set[column], cups_num)
    context['discretized_graphs'] = plots.create_bar_graph(discretized_data_set).items()
    return render(request, 'swdcore/index.html', context)


def knn(request):
    data_set = context['data_set']
    context['metric'] = request.GET['metric']

    mapa = {}
    test_data_set, training_data_set = operations.generate_training_and_test_data(data_set)
    for x in range(1, 91):
        mapa[x] = invoke_knn_algorithm(test_data_set, training_data_set, x, context['metric'])

    create_table_with_data_from_file(data_set, request)
    context['graph_knn'] = plots.create_plot_from_dict(mapa)
    # context['graphs_knn'] = plots.create_knn_plots(data_set, 'markers')
    return render(request, 'swdcore/knn.html', context)


def invoke_knn_algorithm(test_data_set, training_data_set, k, metric):
    # test_data_set, training_data_set = operations.generate_training_and_test_data(data_set)
    knn_alg = KnnAlgorithm(training_data_set, test_data_set, metric, k)
    predicted_classes = knn_alg.run_knn()
    accuracy = knn_alg.calculate_prediction_accuracy(predicted_classes)
    return accuracy


def k_means(request):
    data_set = context['data_set']
    context['metric'] = request.GET['metric']
    metric = request.GET['metric']

    distinct_val, count_distinct = np.unique(data_set.iloc[:, -1], return_counts=True)
    values = data_set.values
    km = K_Means(k=count_distinct.size, metric=metric)
    km.fit(values)
    plots.create_plot_k_means(km)
    return render(request, 'swdcore/k_means.html', context)


def create_form(request):
    if request.method == 'POST':
        form = SWDForm(request.POST)
        if form.is_valid():
            pass
    else:
        form = SWDForm()
    context['form'] = form


def create_table_with_data_from_file(data_set, request):
    dict_data = [{col: row[idx + 1] for idx, col in enumerate(data_set)} for row in data_set.itertuples()]
    table = CsvFileTable(dict_data, extra_columns=((column_x, tables.Column()) for column_x in data_set.columns))
    context['table'] = table
    RequestConfig(request, paginate={'per_page': 40}).configure(table)
