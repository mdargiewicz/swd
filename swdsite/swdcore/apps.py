from django.apps import AppConfig


class SwdcoreConfig(AppConfig):
    name = 'swdcore'
