from django import forms
from django.conf import settings


class SWDForm(forms.Form):
    CHOICES_SEP = (
        ('\t', 'Tabulator'),
        (' ', 'Spacja'),
        (',', 'Przecinek')
    )
    CHOICES_HEADER = (
        ('True', 'Yes'),
        ('False', 'No')
    )
    file = forms.FilePathField(label='File', path=settings.FILE_PATH_FIELD_DIRECTORY)
    separator = forms.ChoiceField(choices=CHOICES_SEP)
    has_header = forms.ChoiceField(choices=CHOICES_HEADER)

    def clean(self):
        cleaned_data = super(SWDForm, self).clean()
        file = cleaned_data.get('imported_file')
        separator = cleaned_data.get('separator')
        has_header = cleaned_data.get('has_header')
        if not file and not separator and not has_header:
            raise forms.ValidationError('Take default values')
